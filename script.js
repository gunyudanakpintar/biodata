function showError(inputId, message) {
    const errorElement = document.getElementById(inputId + 'Error');
    errorElement.textContent = message;
    errorElement.style.display = 'block';
}

function hideError(inputId) {
    const errorElement = document.getElementById(inputId + 'Error');
    errorElement.textContent = '';
    errorElement.style.display = 'none';
}

function validateForm() {
    let isValid = true;

    // Validate name
    const name = document.getElementById('name').value;
    if (name.trim() === '') {
        showError('name', 'Nama harus diisi.');
        isValid = false;
    } else {
        hideError('name');
    }

    // Validate age
    const age = document.getElementById('age').value;
    if (age.trim() === '' || isNaN(age) || age < 1 || age > 120) {
        showError('age', 'Umur harus antara 1 dan 120.');
        isValid = false;
    } else {
        hideError('age');
    }

    // Validate gender
    const gender = document.getElementById('gender').value;
    if (gender === '') {
        showError('gender', 'Jenis kelamin harus dipilih.');
        isValid = false;
    } else {
        hideError('gender');
    }

    // Validate birthdate
    const birthdate = document.getElementById('birthdate').value;
    if (birthdate.trim() === '') {
        showError('birthdate', 'Tanggal lahir harus diisi.');
        isValid = false;
    } else {
        hideError('birthdate');
    }

    // Validate email
    const email = document.getElementById('email').value;
    const emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
    if (!emailPattern.test(email)) {
        showError('email', 'Email tidak valid.');
        isValid = false;
    } else {
        hideError('email');
    }

    // Validate phone
    const phone = document.getElementById('phone').value;
    const phonePattern = /^[0-9]{10,15}$/;
    if (!phonePattern.test(phone)) {
        showError('phone', 'Nomor telepon tidak valid.');
        isValid = false;
    } else {
        hideError('phone');
    }

    // Validate address
    const address = document.getElementById('address').value;
    if (address.trim() === '') {
        showError('address', 'Alamat harus diisi.');
        isValid = false;
    } else {
        hideError('address');
    }

    return isValid;
}

function submitForm() {
    if (validateForm()) {
        const name = document.getElementById('name').value;
        const age = document.getElementById('age').value;
        const gender = document.getElementById('gender').value;
        const birthdate = document.getElementById('birthdate').value;
        const email = document.getElementById('email').value;
        const phone = document.getElementById('phone').value;
        const address = document.getElementById('address').value;

        const resultDiv = document.getElementById('result');
        resultDiv.innerHTML = `
            <h2>Hasil Biodata</h2>
            <p><strong>Nama:</strong> ${name}</p>
            <p><strong>Umur:</strong> ${age}</p>
            <p><strong>Jenis Kelamin:</strong> ${gender === 'male' ? 'Laki-laki' : 'Perempuan'}</p>
            <p><strong>Tanggal Lahir:</strong> ${birthdate}</p>
            <p><strong>Email:</strong> ${email}</p>
            <p><strong>Nomor Telepon:</strong> ${phone}</p>
            <p><strong>Alamat:</strong> ${address}</p>
        `;
    }
}
